module BolteamConnection

	mattr_accessor :mode, :api_url
	@@mode = "demo"
	@@api_url = "api.demo.bolteam.mx"

  class << self

		def config
			yield self
    end


		def url
			if @@mode.to_s == "production"
				return "api.bolteam.mx"
			else
				return @@api_url
			end
		end
  end



	class Connection
		attr_accessor :api_key, :api_secret, :url_origin

		def initialize(attrs = {})
		  @api_key = attrs[:api_key]
  		@api_secret = attrs[:api_secret]
			@url_origin = ::BolteamConnection.url
		end

    def connect(method, target_url, params = {})
      full_url = "http://#{self.api_key}:#{self.api_secret}@#{self.url_origin}/v1/#{target_url}"
      puts "Request to ==>  #{full_url}"
      puts "With params ==>  #{params}"

      begin
        if params[:csd]
          response = RestClient.post(full_url, params)
        else
          response = eval("::RestClient.#{method}('#{full_url}', #{params})")
        end
      rescue => e
				response = e.response
      end

			return JSON.parse(response)

    end

	end
end


require "bolteam_connection/version"
require 'bolteam_connection/branch'
require 'bolteam_connection/client'
require 'bolteam_connection/client_branch'
require 'bolteam_connection/company'
require 'bolteam_connection/invoice'
require 'bolteam_connection/user'
require "rest_client"
