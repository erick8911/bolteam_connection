module BolteamConnection
  class Connection

    def get_companies
      connect("get", companies_index_url)
    end

    def create_company(attrs = {})
      if !attrs.keys.include?("branch_offices_attributes[]") && !attrs.keys.include?("branch_offices_attributes") #Si no vienen attributos de branch office tomamos los datos de la empresa
        attrs = attrs.merge("branch_offices_attributes[]" => [{name: "Matriz",street: attrs[:street], city: attrs[:city],
            country: attrs[:country], neighborhood: attrs[:neighborhood], external_number: attrs[:external_number], zip_code: attrs[:zip_code], state: attrs[:state]}]
          )
      end
      connect("post", companies_index_url, company: attrs)
    end

    def get_company(company_id)
      connect("get", companies_show_url(company_id))
    end

    def update_company(company_id, attrs)
      connect("patch", companies_show_url(company_id), company: attrs)
    end


    private
      def companies_index_url
        "companies"
      end

      def companies_show_url(company_id)
        "companies/#{company_id}"
      end

  end
end
