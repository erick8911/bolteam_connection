module BolteamConnection
  class Connection

    def get_clients(company_id, branch_id)
      connect("get", clients_index_url(company_id, branch_id))
    end

    def create_client(company_id, branch_id, attrs = {})
      if !attrs.keys.include?("branch_offices_attributes[]") && !attrs.keys.include?("branch_offices_attributes") #Si no vienen attributos de branch office tomamos los datos de la empresa
        attrs = attrs.merge("branch_offices_attributes[]" => [{name: "Matriz",street: attrs[:street], city: attrs[:city],
            country: attrs[:country], neighborhood: attrs[:neighborhood], external_number: attrs[:external_number], zip_code: attrs[:zip_code], state: attrs[:state]}]
          )
      end
      connect("post", clients_index_url(company_id, branch_id), client: attrs)
    end

    def get_client(company_id, branch_id, client_id)
      connect("get", clients_show_url(company_id, branch_id, client_id))
    end

    def update_client(company_id, branch_id, client_id, attrs)
      connect("patch", clients_show_url(company_id, branch_id, client_id), client: attrs)
    end


    private
      def clients_index_url(company_id, branch_id)
        "companies/#{company_id}/branch_offices/#{branch_id}/clients"
      end

      def clients_show_url(company_id, branch_id, client_id)
        "companies/#{company_id}/branch_offices/#{branch_id}/clients/#{client_id}"
      end

  end
end
