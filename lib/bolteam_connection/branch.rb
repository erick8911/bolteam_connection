module BolteamConnection
  class Connection

    def get_branches(company_id)
      connect("get", branches_index_url(company_id))
    end

    def create_branch(company_id, attrs = {})
      connect("post", branches_index_url(company_id), branch_office: attrs)
    end

    def get_branch(company_id, branch_id)
      connect("get", branches_show_url(company_id, branch_id) )
    end

    def update_branch(company_id, branch_id, attrs = {})
      connect("patch", branches_show_url(company_id, branch_id), branch_office: attrs)
    end

    def add_user(company_id, branch_id, attrs = {})
      connect("post", branches_show_url(company_id, branch_id) + "/add_user", attrs)
    end

    def delete_user(company_id, branch_id, attrs = {})
      connect("post", branches_show_url(company_id, branch_id) + "/delete_user", attrs)
    end

    #Custom
    def branch(branch_id) #show
      connect("get", "branch_offices/#{branch_id}")
    end


    def load_csd(company_id, branch_id, attrs = {})
      attrs[:cer_file] = File.new(attrs[:cer_file]) if attrs[:cer_file] && attrs[:cer_file].class == String
      attrs[:key_file] = File.new(attrs[:key_file]) if attrs[:key_file] && attrs[:key_file].class == String

      connect("post", branches_show_url(company_id, branch_id) + "/load_csd", csd: attrs)
    end

    def branch_public_info(branch_id)
      connect("get", "branch_offices/#{branch_id}/info")
    end


    private

      def branches_index_url(company_id)
        "companies/#{company_id}/branch_offices"
      end

      def branches_show_url(company_id, branch_id)
        "companies/#{company_id}/branch_offices/#{branch_id}"
      end

  end
end
