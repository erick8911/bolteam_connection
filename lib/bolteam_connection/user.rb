module BolteamConnection
  class Connection

    def test_connection
      connect("get", "test-connection")
    end  

    def show_user
      connect("get", "users/user_info")
    end   

    private


    def users_show_url(user_id) 
      "users/#{user_id}"
    end    


  end
end