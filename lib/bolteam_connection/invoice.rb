module BolteamConnection
  class Connection

    def create_invoice(company_id, branch_office_id, client_id, branch_client_id, attrs)
      connect("post", invoices_index_url(company_id, branch_office_id), invoice: attrs.merge(client_id: client_id, client_branch_office_id:branch_client_id))
    end

    def get_invoices(company_id, branch_office_id)
      connect("get", invoices_index_url(company_id, branch_office_id))
    end

    def get_invoice(company_id, branch_office_id, invoice_id)
      connect("get", invoices_show_url(company_id, branch_office_id, invoice_id))
    end

    def cancel_invoice(company_id, branch_office_id, invoice_id)
      connect("post", invoices_show_url(company_id, branch_office_id, invoice_id) + "/cancel" )
    end


    private

      def invoices_index_url(company_id, branch_office_id)
        "companies/#{company_id}/branch_offices/#{branch_office_id}/invoices"
      end

      def invoices_show_url(company_id, branch_office_id, invoice_id)
        "companies/#{company_id}/branch_offices/#{branch_office_id}/invoices/#{invoice_id}"
      end

  end
end
