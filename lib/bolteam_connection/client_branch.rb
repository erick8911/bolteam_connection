module BolteamConnection
  class Connection

    def get_client_branches(company_id, branch_id, client_id)
      connect("get", client_branches_index_url(company_id, branch_id, client_id))
    end

    def create_client_branch(company_id, branch_id, client_id, attrs = {})
      connect("post", client_branches_index_url(company_id, branch_id, client_id), branch_office: attrs)
    end

    def get_client_branch(company_id, branch_id, client_id, branch_client_id)
      connect("get", client_branches_show_url(company_id, branch_id, client_id, branch_client_id))
    end

    def update_client_branch(company_id, branch_id, client_id, branch_client_id, attrs)
      connect("patch", client_branches_show_url(company_id, branch_id, client_id, branch_client_id), branch_office: attrs)
    end

    def add_client(company_id, branch_id, client_id, branch_client_id)
      connect("post", client_branches_show_url(company_id, branch_id, client_id, branch_client_id) + "/add_client")
    end


    private

      def client_branches_index_url(company_id, branch_id, client_id)
        "companies/#{company_id}/branch_offices/#{branch_id}/clients/#{client_id}/client_branches"
      end

      def client_branches_show_url(company_id, branch_id, client_id, branch_client_id)
        "companies/#{company_id}/branch_offices/#{branch_id}/clients/#{client_id}/client_branches/#{branch_client_id}"
      end

  end
end
