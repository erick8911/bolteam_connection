# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'bolteam_connection/version'

Gem::Specification.new do |spec|
  spec.name          = "bolteam_connection"
  spec.version       = BolteamConnection::VERSION
  spec.authors       = ["Erick"]
  spec.email         = ["erick_8911@hotmail.com"]
  spec.description   = "Bolteam integration"
  spec.summary       = "Ruby wrapper api"
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency "rest-client", '~> 1.6.7'
  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "rake"
end
