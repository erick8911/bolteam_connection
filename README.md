# BolteamConnection

Conexión a [bolteam.mx](https://www.bolteam.mx/)

## Installation

Add this line to your application's Gemfile:

    gem 'bolteam_connection'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install bolteam_connection

## Conexión

Ingresar a [bolteam.mx](https://www.bolteam.mx/) y obtener las credenciales de usuario en el apartado de "Ajustes de usuario"


Conectar usuario:

    conexion = BolteamConnection::Connection.new(api_key: user_api_key, api_secret: user_api_secret)


El url del api por default es [api.demo.bolteam.mx](api.demo.bolteam.mx) para pasar a modo de producción se requiere
la siguiente configuración.

      #initializers/bolteam_connection.rb
      BolteamConnection.config do |config|
        config.mode = "production"
      end




## Empresas

Listado de empresas del usuario

    conexion.get_companies

###Crear empresa

Atributos de la nueva empresa

    attrs =  {
      name: "Nombre de la empresa",
      street: "Calle",
      external_number: "100",
      neighborhood: "Colonia",
      zip_code: "12345",
      city: "monterrey",
      state: "Nuevo leon",
      rfc: "ets130716i11",
      country: "mx" ,
      billing_address_email: "nueva-empresa@empresa.com",
      company_policy_id: 1,

      #Opcionales. Si no tiene los datos de la sucursal se toman los datos fiscales.

      "branch_offices_attributes" => [
        { "1" => { name: "Matriz", street: "Calle", city: "San Pedro Garza Garcia", country: "Mx", neighborhood: "Valle ote", external_number: "100", zip_code: "12345", state: "Nuevo leon"}},

        { "2" => { name: "Sucursal 001", street: "Calle", city: "Monterrey", country: "Mx", neighborhood: "Mitras", external_number: "100", zip_code: "12345", state: "Nuevo leon"}},  
      ]
    }

Request para crear empresa

    conexion.create_company(attrs)



Regimen de las empresas (company_policy_id):

  * 1 - Persona Moral
  * 2 - Persona Moral con fines lucrativos
  * 3 - Persona Física
  * 4 - Persona Física con Actividad Empresarial
  * 5 - Régimen de Pequeños Contribuyentes




### Sucursales de empresa

Ver sucursales de la empresa

    conexion.get_branches(company_id)


Ver sucursal especifica:

    conexion.get_branch(company_id, branch_id)


Shortcut

    conexion.branch(branch_id)

Cargar CSD:

    attrs = {
      key_password: "123345678a",  #Password del CSD
      key_password_confirmation: "123345678a",  
      serie: "AA",
      folio: "1",
      cer_file: archivo.cer,
      key_file: archivo.key
    }

    conexion.load_csd(company_id, branch_id, attrs)


## Clientes de la sucursal

Listado de clientes

    conexion.get_clients(company_id, branch_id)

### Crear cliente

Atributos del nuevo cliente


```ruby
    attrs =  {
      name: "Nombre de la empresa",
      street: "Calle",
      external_number: "100",
      neighborhood: "Colonia",
      zip_code: "12345",
      city: "monterrey",
      state: "Nuevo leon",
      rfc: "ets130716i11",
      country: "mx" ,
      billing_address_email: "nueva-empresa@empresa.com",
      company_policy_id: 1,

      #Opcionales. Si no tiene los datos de la sucursal se toman los datos fiscales.

      "branch_offices_attributes" => [
        { "1" => { name: "Matriz", street: "Calle", city: "San Pedro Garza Garcia", country: "Mx", neighborhood: "Valle ote", external_number: "100", zip_code: "12345", state: "Nuevo leon"}},

        { "2" => { name: "Sucursal 001", street: "Calle", city: "Monterrey", country: "Mx", neighborhood: "Mitras", external_number: "100", zip_code: "12345", state: "Nuevo leon"}},  
      ]
    }
```

Request para crear cliente:

    conexion.create_client(company_id, branch_id, attrs)


### Sucursales

Ver sucursales del cliente

    conexion.get_client_branches(company_id, branch_id, client_id)


### Ver informacion de sucursal

    conexion.get_client_branch(company_id, branch_id, client_id, client_branch_id)

### Agregar sucursal existente como cliente:

    conexion.add_client(company_id, branch_id, client_id, branch_client_id)


## Generar factura

Atributos de la factura

    attrs = {
      payment_type_id: 1,
      payment_condition_id: 1,
      currency_id: 1,
      payment_method_id: 1,
      expiration_date: "2014-03-20",
      iva_percentage: "16",
      invoice_type_id: 1,
      "invoice_details_attributes" => [
        { "1" => {product_description: "Un producto", product_quantity: "1", product_unit_price: "100", product_units: "Litros"}},
        { "2" => {product_description: "Otro producto", product_quantity: "1", product_unit_price: "200", product_units: "Litros"}}
      ]
    }


Request

    conexion.create_invoice(company_id, branch_office_id, client_id, branch_client_id, attrs)




### Catalogos de factura


Tipo de pago (payment_type_id):

  * 1 - Pago en una sola exhibición

Condiciones de pago (payment_condition_id):

  * 1 - Contado
  * 2 - Crédito

Moneda (currency_id):

  * 1 - MXN
  * 2 - USD
  * 3 - EUR

Metodo de pago (payment_method_id):


  * 1 - Transferencia electrónica
  * 2 - Efectivo
  * 3 - Cheque
  * 4 - Tarjeta Crédito/Débito
  * 5 - No definido
  * 6 - No aplica


Tipo de factura (invoice_type_id):

  * 1 - Factura
  * 2 - Recibo de arrendamiento
  * 3 - Recibo de donativos
  * 4 - Nota de credito
  * 5 - Nota de cargo
  * 6 - Recibo de nomina
  * 7 - Recibo de honorarios
  * 8 - Carta porte
  * 9 - Pedimentos aduanales

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
